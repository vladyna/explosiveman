﻿using AlahAkbarMan.Game;
using AlahAkbarMan.Game.Entity;
using AlahAkbarMan.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AlahAkbarMan
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Input input             = new Input();
       // Field field             = new Field();
        PlayersInstance players = new PlayersInstance();
        public static Grid CurrentGrid;
     
        public MainWindow()
        {
            InitializeComponent();
            CurrentGrid             =   FieldGrid;
            KeyDown                 +=  input.OnKeyDownHandler;
            KeyDown                 += ShowPlayerPosition;

            Field.Instance.GenerateField();
            Field.Instance.AddToFieldToAGrid(CurrentGrid);
            players.InstatiatePlayers();
        }

        public void ShowPlayerPosition(object sender, KeyEventArgs e)
        {
            Debuger_textBlock.Text = Debuger.PrintLog();
            Debuger.RefreshLog();
        }

    }
}
