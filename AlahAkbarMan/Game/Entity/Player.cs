﻿using AlahAkbarMan.Game.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Numerics;
using Extensions;

namespace AlahAkbarMan.Game.Entity
{
    public class Player : Entity
    {
       /// public Button PlayerObject;
        public Bomb Bomb;
        Vector2 direction;
        Vector2 _shiftDirection = Vector2.Zero;
        Vector2 _shiftPosition  = Vector2.Zero;
        public Player(Vector2 fieldPosition) : base(fieldPosition)
        {
            //this._uIPosition = uiPosition;
  
          //  Bomb.SetMaxSize(32f, 32f);
           // direction = new Vector2();
            Input.RightKeyDown  += () => { direction = direction.Right();   Move(); };
            Input.LeftKeyDown   += () => { direction = direction.Left();    Move(); };
            Input.DownKeyDown   += () => { direction = direction.Up();      Move(); };
            Input.UpKeyDown     += () => { direction = direction.Down();    Move(); };
            Input.SpaceKeyDown  += () => { Bomb = new Bomb(_fieldPosition); };
            SetUpImage(new Uri(@"Assets\Sprites\Player.png", UriKind.RelativeOrAbsolute));
            // MainWindow.CurrentGrid
            Panel.SetZIndex(_objectRect, 1000);
           
            //objectImage.Name = "Player";
        }

        public override void Draw()
        {
            throw new NotImplementedException();
        }

        public override void Move()
        {
            Debuger.Log = "Rect Position" + _objectRect.ToString();
            MoveUIElement(direction.X * Speed, direction.Y * Speed);
            if (Field.Instance.CheckCollision(this, direction))
            {
               // if (!Shift())
                    MoveUIElement(-direction.X * Speed, -direction.Y * Speed);

            }
        }
        private bool Shift()
        {
            if (direction == _shiftDirection)
            {
                Vector2 moveShift = (_shiftPosition - _fieldPosition) * Speed;
                MoveUIElement(moveShift.X, moveShift.Y);
                    return true;
            }
            _shiftDirection = Vector2.Zero;
            _shiftPosition  = Vector2.Zero;
            return false;
        }
        public void ShiftTo(Vector2 position, Vector2 direction)
        {
            _shiftPosition  = position;
            _shiftDirection = direction;
        }

        public override void Update()
        {
 
        }

        private void MoveUIElement(float x, float y)
        {
            _objectRect.Margin   = new Thickness(_objectRect.Margin.Left + x, _objectRect.Margin.Top + y, 0f, 0f);
            _uIPosition          = new Vector2((float)_objectRect.Margin.Left, (float)_objectRect.Margin.Top);

            float x1 = (_uIPosition.X / 32f);
            float y1 = (_uIPosition.Y / 32f);
            _fieldPosition = new Vector2((float)Math.Round(x1), (float)Math.Round(y1));
            //_objectRect.Margin = objectImage.Margin;
        }
    }
}
