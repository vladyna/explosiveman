﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace AlahAkbarMan.Game.Entity
{
    public abstract class Entity : GameObject
    {
        protected Entity(Vector2 fieldPosition) : base(fieldPosition)
        {

        }
        private Vector2 realPosition;
        private float   speed;

        public float    Speed        { get { return speed; } set { speed = value; } }
        public Vector2  RealPosition { get { return realPosition; } }
       
        public abstract override void   Update();
        public abstract override void   Draw();
        public abstract void            Move();

    }
}
