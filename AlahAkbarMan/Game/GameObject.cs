﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows;
using System.Numerics;

namespace AlahAkbarMan.Game
{
    public abstract class GameObject
    {   
       
        protected GameObject(Vector2 fieldPosition)
        {
            _fieldPosition  = fieldPosition;
            _uIPosition     = fieldPosition * Field.Instance.size;
           
        }
        
        protected   Vector2     _fieldPosition;
        protected   Vector2     _uIPosition;
        protected   Rectangle   _objectRect;
        protected int offset = 5;
        public      Vector2 FieldPosition   { get { return _fieldPosition; } set { _fieldPosition = value; } }
        public      Vector2 UIPosition      { get { return _uIPosition; }    set { _uIPosition = value; } }
        public      Rectangle ObjectRectangle    { get { return _objectRect;  } }
        public      Rect RectField { get { return new Rect(_uIPosition.X - offset, _uIPosition.Y - offset, _objectRect.Width - offset, _objectRect.Height - offset); } }
        public abstract void Update();
        public abstract void Draw();

        
        protected void SetUpImage(Uri imageUri)
        {
            _objectRect                         = new Rectangle();
            _objectRect.Height                  = 32;
            _objectRect.Width                   = 32;
            _objectRect.Margin                  = new Thickness(_uIPosition.X, _uIPosition.Y, 0f, 0f);
            _objectRect.HorizontalAlignment     = HorizontalAlignment.Left;
            _objectRect.VerticalAlignment       = VerticalAlignment.Top;
            ImageBrush brush                    = new ImageBrush();
            brush.ImageSource                   = new BitmapImage(imageUri);
            _objectRect.Fill                    = brush;
        }

        public void ShowPosition()
        {
            Debuger.Log = $"X:{RectField.X} Y:{RectField.Y} Width:{RectField.Width} Height:{RectField.Height}";
        }
    }
}
