﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Numerics;
using System.Timers;


namespace AlahAkbarMan.Game.Item
{
    public class Bomb : Item, ICollidable, IDestroyable
    {
        private float _explosionRadius = 2;
        public float ExplosionRadius { get { return _explosionRadius; } set { _explosionRadius = value; } }

        private Explosion   _explosion;
        private Timer       _timetoBlowUp;
        private Timer       _explosionSpreadingTime;

        public Bomb(Vector2 fieldPosition) : base(fieldPosition)
        {
            //_uIPosition = fieldPosition;
            SetUpImage(new Uri(@"Assets\Sprites\pokeball.png", UriKind.RelativeOrAbsolute));

            MainWindow.CurrentGrid.Children.Add(_objectRect);

            Field.Instance.gameObjects.Add(this);

            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(5000);
                BlowUp();
            });
        }

        public void BlowUp()
        {
            Destroy();
            _explosion = new Explosion(null, _fieldPosition, _explosionRadius);
           // Task.Factory.StartNew(() =>
          //  {
            //    System.Threading.Thread.Sleep(500);
            _explosion.StartSpreading();
            // });
      
        }
        
        public override void Draw()
        {
            throw new NotImplementedException();
        }

        public override void Update()
        {
            throw new NotImplementedException();
        }

        public bool CheckCollision(GameObject obj)
        {
            ShowPosition();
            return RectField.IntersectsWith(obj.RectField);
        }

        public void Destroy()
        {
            MainWindow.CurrentGrid.Dispatcher.BeginInvoke(new Action(() => {
                MainWindow.CurrentGrid.Children.Remove(_objectRect);
                Field.Instance.gameObjects.Remove(this);
            })).Wait();

        }
    }


}
