﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Extensions;
using System.Windows.Threading;
using System.Windows;

public enum ExplosionDirections : byte
{
    Up      = 0x01,
    Down    = 0x02,
    Right   = 0x04,
    Left    = 0x08
}

namespace AlahAkbarMan.Game.Item
{
    public class Explosion : Item, ICollidable, IDestroyable
    {
        private ExplosionDirections _explosionDirection;
        private float _explosionRadius;
        public Explosion(Explosion prevExplosion, Vector2 fieldPosition, float explosionRadius, ExplosionDirections explosionDirections = ExplosionDirections.Down | ExplosionDirections.Left | ExplosionDirections.Right | ExplosionDirections.Up) : base(fieldPosition)
        {
            _explosionRadius        = explosionRadius;
            _explosionDirection     = explosionDirections;

            MainWindow.CurrentGrid.Dispatcher.BeginInvoke(new Action(() =>
                {
                    SetUpImage(new Uri(@"Assets\Sprites\Boom!.png", UriKind.RelativeOrAbsolute));
                    MainWindow.CurrentGrid.Children.Add(_objectRect);
                    GameObject temp;
                    if (((temp = Field.Instance.Contains(_fieldPosition)) as ICollidable)?.CheckCollision(this) ?? false || temp is IDestroyable)
                    {
                        (temp as IDestroyable)?.Destroy();
                        this.Destroy();
                        _explosionDirection ^= explosionDirections;
                    }
                    else
                        Field.Instance.gameObjects.Add(this);
                }
                )).Wait();         
        }


        public void StartSpreading()
        {
            Explosion explosion;
            
            if(_explosionRadius != 0)
            {
                    _explosionRadius--;
                    if((_explosionDirection & ExplosionDirections.Down) == ExplosionDirections.Down)
                    {
                        explosion = new Explosion(this, _fieldPosition + _fieldPosition.Down(), _explosionRadius, ExplosionDirections.Down);
                        explosion.StartSpreading();
        
                    }
                    if ((_explosionDirection & ExplosionDirections.Up) == ExplosionDirections.Up)
                    {
                        explosion = new Explosion(this, _fieldPosition + _fieldPosition.Up(), _explosionRadius, ExplosionDirections.Up);
                        explosion.StartSpreading();
                    }
                    if ((_explosionDirection & ExplosionDirections.Right) == ExplosionDirections.Right)
                    { 
                        explosion = new Explosion(this, _fieldPosition + _fieldPosition.Right(), _explosionRadius, ExplosionDirections.Right);
                        explosion.StartSpreading();  
                    }
                    if ((_explosionDirection & ExplosionDirections.Left) == ExplosionDirections.Left)
                    {
                        explosion = new Explosion(this, _fieldPosition + _fieldPosition.Left(), _explosionRadius, ExplosionDirections.Left);
                        explosion.StartSpreading();
                    }
            }
           
        }

        public override void Draw()
        {
            throw new NotImplementedException();
        }

        public override void Update()
        {
            throw new NotImplementedException();
        }

        public bool CheckCollision(GameObject obj)
        {
            return RectField.IntersectsWith(obj.RectField);
        }

        public void Destroy()
        {
            MainWindow.CurrentGrid.Children.Remove(_objectRect);
            Field.Instance.gameObjects.Remove(this);
        }
    }
}
