﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Numerics;

namespace AlahAkbarMan.Game.Item
{
    public abstract class Item : GameObject
    {
        protected Item(Vector2 fieldPosition) : base(fieldPosition)
        {

        }

        protected Button itemObject;
        protected float height;
        protected float width;

        public void SetMaxSize(float maxHeight, float maxWidth)
        {
            this.height = maxHeight;
            this.width = maxWidth;
        }

        public Button ItemObject { get { return itemObject; } }
        public abstract override void Update();
        public abstract override void Draw();

    }
}
