﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace AlahAkbarMan.Game.Obstacles
{
    public abstract class Obstacle : GameObject
    {
        protected Obstacle(Vector2 fieldPosition) : base(fieldPosition)
        {

        }
        public Button obstacleObject;
        public override abstract void Draw();

        public override abstract void Update();
    }
}
