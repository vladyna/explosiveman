﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Numerics;

namespace AlahAkbarMan.Game.Obstacles
{
    public class Wall : Obstacle, ICollidable
    {
        public Wall(Vector2 fieldPosition) : base(fieldPosition)
        {
            SetUpImage(new Uri(@"Assets\Sprites\tileSet\Wall.png", UriKind.RelativeOrAbsolute));
        }

        public bool CheckCollision(GameObject obj)
        {

            ShowPosition();
            return RectField.IntersectsWith(obj.RectField);
        }

        public override void Draw()
        {
            throw new NotImplementedException();
        }

        public override void Update()
        {
            throw new NotImplementedException();
        }
    }
}
