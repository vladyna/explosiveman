﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace AlahAkbarMan.Game.Obstacles
{
    public class Ground : Obstacle
    {
        public Ground(Vector2 fieldPosition) : base(fieldPosition)
        {
           // this._uIPosition = uiPosition;
            SetUpImage(new Uri(@"Assets\Sprites\tileSet\Ground.png", UriKind.RelativeOrAbsolute));
        }

        public override void Draw()
        {
            throw new NotImplementedException();
        }

        public override void Update()
        {
            throw new NotImplementedException();
        }
    }
}
