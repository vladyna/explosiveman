﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

public delegate void RightPointerDown();
public delegate void LeftPointerDown();
public delegate void DownPointerDown();
public delegate void UpPointerDown();
public delegate void SpaceDown();

namespace AlahAkbarMan.Game
{

    public class Input
    {
        public static event RightPointerDown RightKeyDown;
        public static event RightPointerDown LeftKeyDown;
        public static event RightPointerDown DownKeyDown;
        public static event RightPointerDown UpKeyDown;
        public static event RightPointerDown SpaceKeyDown;
        public static Key KeyPressed;

        public void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            KeyPressed = e.Key;
            if (e.Key == Key.Right)
            {
                RightKeyDown.Invoke();
            }
            else if (e.Key == Key.Left)
            {
                LeftKeyDown.Invoke();
            }
            else if (e.Key == Key.Down)
            {
                DownKeyDown.Invoke();
            }
            else if (e.Key == Key.Up)
            {
                UpKeyDown.Invoke();
            }
            else if (e.Key == Key.Space)
            {
                SpaceKeyDown.Invoke();
            }

        }
    }
}
