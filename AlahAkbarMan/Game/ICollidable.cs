﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlahAkbarMan.Game
{
    public interface ICollidable
    {
        bool CheckCollision(GameObject obj);
    }
}
