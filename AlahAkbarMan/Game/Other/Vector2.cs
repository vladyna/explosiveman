﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

    //public class Vector2
    //{
    //    private float x, y;
    //    public float X { get { return x; } }
    //    public float Y { get { return y; } }

    //    public static Vector2 Left  { get { return new Vector2(-1f, 0f); } }
    //    public static Vector2 Right { get { return new Vector2(1f, 0f); } }
    //    public static Vector2 Up    { get { return new Vector2(0f, 1f); } }
    //    public static Vector2 Down  { get { return new Vector2(0f, -1f); } }
    //    public static Vector2 Zero { get { return new Vector2(0f, 0f); } }

    //    public Vector2()
    //    {
    //        x = 0;
    //        y = 0;
    //    }

    //    public Vector2(float x, float y)
    //    {
    //        this.x = x;
    //        this.y = y;
    //    }

    //    public Vector2(Vector2 current)
    //    {
    //        this.x = current.x;
    //        this.y = current.y;
    //    }

    //    public Vector2 Set(float x, float y)
    //    {
    //        return new Vector2(x, y);
    //    }

    //    public Vector2 Set(Vector2 current)
    //    {
    //        return new Vector2(current.x, current.y);
    //    }


    //    public void Reset()
    //    {
    //        x = 0;
    //        y = 0;
    //    }

    //    public static Vector2 operator*(Vector2 current, float value)
    //    {
    //        return new Vector2(current.x * value, current.y * value);
    //    }

    //    public static Vector2 operator +(Vector2 current, float value)
    //    {
    //        return new Vector2(current.x + value, current.y + value);
    //    }

    //    public static Vector2 operator +(Vector2 current, Vector2 value)
    //    {
    //        return new Vector2(current.x + value.x, current.y + value.y);
    //    }

    //    public static Vector2 operator -(Vector2 current, Vector2 value)
    //    {
    //        return new Vector2(current.x - value.x, current.y - value.y);
    //    }

    //    public static Vector2 operator /(Vector2 current, float value)
    //    {
    //        return new Vector2(current.x / value, current.y / value);
    //    }
    //    public static Vector2 operator /(Vector2 current, Vector2 value)
    //    {
    //        return new Vector2(current.x / value.x, current.y / value.y);
    //    }

    //    public override string ToString()
    //    {
    //        return string.Format("X:{0} Y:{1}", X, Y);
    //    }

    //    public static bool operator ==(Vector2 first, Vector2 second)
    //    {
    //        return first.X == second.X && first.Y == second.Y;
    //    }

    //    public static bool operator !=(Vector2 first, Vector2 second)
    //    {
    //        return first.X != second.X && first.Y != second.Y;
    //    }

    //    public static Vector2 RoundVector(Vector2 vector)
    //    {

    //        return new Vector2((float)Math.Round(vector.X), (float)Math.Round(vector.Y));
    //    }

    //    public static bool operator <=(Vector2 first, Vector2 second)
    //    {
    //        return first.X <= second.X && first.Y <= second.Y;
    //    }

    //    public static bool operator >=(Vector2 first, Vector2 second)
    //    {
    //        return first.X >= second.X && first.Y >= second.Y;
    //    }
    //}

namespace Extensions
{
    public static class Vector2Extension
    {
        public static Vector2 Left(this Vector2 vector)
        {
            return vector = new Vector2(-1f, 0f);
        }
        public static Vector2 Right(this Vector2 vector)
        {
            return vector = new Vector2(1f, 0f);
        }
        public static Vector2 Up(this Vector2 vector)
        {
            return vector = new Vector2(0f, 1f);
        }
        public static Vector2 Down(this Vector2 vector)
        {
            return vector = new Vector2(0f, -1f);
        }
        public static Vector2 Zero(this Vector2 vector)
        {
            return vector = new Vector2(0f, 0f);
        }

        public static Vector2 Increase(this Vector2 vector, float value) 
        {
            return new Vector2(vector.X + value, vector.Y + value);
        }
    }
}

