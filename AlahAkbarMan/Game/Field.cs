﻿using AlahAkbarMan.Game.Entity;
using AlahAkbarMan.Game.Obstacles;
using AlahAkbarMan.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Numerics;
using Extensions;

namespace AlahAkbarMan.Game
{
    public class Field
    {
        private static Field _instance;
        public static Field Instance { get { return _instance ?? (_instance = new Field()); } }
        public Vector2 size;

        public List<GameObject> gameObjects;



        string[,] field = new string[,] { { "w", "w", "w", "w", "w", "w", "w", "w", "w", "w" },
                                          { "w", "g", "p", "g", "g", "g", "g", "g", "g", "w" },
                                          { "w", "g", "g", "g", "g", "g", "g", "g", "g", "w" },
                                          { "w", "g", "g", "g", "g", "g", "g", "g", "g", "w" },
                                          { "w", "g", "g", "g", "w", "g", "g", "g", "g", "w" },
                                          { "w", "g", "g", "g", "g", "g", "g", "g", "g", "w" },
                                          { "w", "g", "g", "g", "g", "g", "g", "g", "g", "w" },
                                          { "w", "g", "g", "w", "g", "g", "g", "g", "g", "w" },
                                          { "w", "g", "g", "g", "g", "g", "g", "g", "g", "w" },
                                          { "w", "w", "w", "w", "w", "w", "w", "w", "w", "w" }};
        Field()
        {
            size        = new Vector2();
            size        += new Vector2(32f, 32f);
            gameObjects = new List<GameObject>();
        }

        public void GenerateField()
        {
            int currentElement = 0;
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    switch (field[i,j])
                    {
                        case "w":
                            gameObjects.Add(new Wall  (new Vector2(j, i)));
                            break;
                        case "g":
                            gameObjects.Add(new Ground(new Vector2(j, i))); 
                            break;
                        case "p":
                            field[i, j] = "g";
                            gameObjects.Add(new Ground(new Vector2(j, i)));
                            PlayersInstance.players.Add(new Player  (new Vector2(j, i)));
                            PlayersInstance.players[0].FieldPosition = new Vector2(j, i);
                            break;
                        default:
                            break;
                    }
                    //gameObjects[currentElement].FieldPosition = new Vector2(j, i);
                    currentElement++;
                }
            }
        }

        public void AddToFieldToAGrid(Grid grid)
        {
            foreach (var gameObj in gameObjects)
            {
                grid.Children.Add(gameObj.ObjectRectangle);
            }

            grid.Children.Add(PlayersInstance.players[0].ObjectRectangle);
       
        }

        public bool CheckCollision(Player player, Vector2 direction)
        {
            Vector2 checkingTopPos;
            Vector2 checkingBottomPos;
            Debuger.Log = player.FieldPosition.ToString();

            Vector2 checkingMiddlePos = player.FieldPosition + direction;
            if(direction == direction.Left() || direction == direction.Right())
            {
                checkingTopPos      = checkingMiddlePos + direction.Down();
                checkingBottomPos   = checkingMiddlePos + direction.Up();
            }
            else
            {
                checkingTopPos      = checkingMiddlePos + direction.Left();
                checkingBottomPos   = checkingMiddlePos + direction.Right();
            }

            GameObject gbmiddle = Contains(checkingMiddlePos);
            GameObject gb1      = Contains(checkingTopPos);
            GameObject gb2      = Contains(checkingBottomPos);

            player.ShowPosition();

            bool? isCollide =   (gbmiddle as ICollidable)?.CheckCollision(player) ?? 
                                (gb1 as ICollidable)?.CheckCollision(player) ??
                                (gb2 as ICollidable)?.CheckCollision(player);

            return isCollide ?? false;
        }

        public GameObject Contains(Vector2 fieldposition)
        {
            foreach (var gameobj in gameObjects)
            {
                if (gameobj.FieldPosition == fieldposition && (gameobj is ICollidable))
                {
                    return gameobj;
                }
            }
            return null;  
        }
    }
}
