﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace AlahAkbarMan
{
    public class Debuger
    {
        private static StringBuilder _log = new StringBuilder("");
        public static string Log { get { return _log.ToString(); } set { _log.AppendLine(value); } }

        public static void RefreshLog()
        {
            _log.Clear();
        }

        public static string PrintLog()
        {
           return Log;

        }
    }
}
